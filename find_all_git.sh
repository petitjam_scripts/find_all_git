#!/bin/bash

FIND="find $HOME"

IGNORE_RVM="-name \".rvm\" -prune -o"
IGNORE_TMUX="-name \".tmux\" -prune -o"
IGNORE_LOCAL="-name \".local\" -prune -o"

FIND_GIT="-name \".git\" -print"

REMOVE_GIT_DIR="sed 's/\.git$//'"

eval "$FIND $IGNORE_RVM $IGNORE_TMUX $IGNORE_LOCAL $FIND_GIT | $REMOVE_GIT_DIR"
